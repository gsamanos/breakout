ARCHITECTURE rtl OF debounce IS

   SIGNAL s_in_current1, s_out_next1,
          s_in_current2, s_out_next2,
          s_in_current3, s_out_next3,
          s_in_current4, s_button_out,
          s_clear, s_reset, s_edge_detect,
          s_A, s_B
          : std_logic;
BEGIN
    s_in_current1 <= button_in AND NOT s_clear;
    s_in_current2 <= s_out_next1 AND NOT s_clear;
    s_in_current3 <= s_out_next2 AND NOT s_clear;
    s_A <= s_out_next2 AND NOT s_out_next3;
    s_B <= s_A OR s_button_out;
    s_in_current4 <=  s_B AND NOT s_clear;
    s_edge_detect <= s_A;

    debouncer : PROCESS (clk)
    BEGIN
    if n_reset = '0' then
        s_in_current1 <= '0';
        s_in_current2 <= '0';
        s_in_current3 <= '0';
        s_in_current4 <= '0';
        
        IF rising_edge(clk) THEN
            s_out_next1 <= s_in_current1;
            s_out_next2 <= s_in_current2;
            s_out_next3 <= s_in_current3;
            s_button_out <= s_in_current4;
            END IF;

        end if;
    END PROCESS debouncer;

    timer_ctrl : PROCESS (clk, n_reset)
    BEGIN
    IF rising_edge(clk) THEN
        IF s_edge_detect = '1' THEN
            s_reset <= '0';
        ELSIF s_clear = '1' THEN
            s_reset <= '1';
        END IF;
    END IF;
    END PROCESS timer_ctrl;

    timer : ENTITY work.timer
    GENERIC MAP (count => debounce_count)
        PORT MAP (
            clk => clk,
            n_reset => n_reset,
            reset => s_reset,
            flag => s_clear
        );
    
     button_out <= s_button_out;

END rtl;