ARCHITECTURE rtl OF display IS

SIGNAL s_led_array : STD_LOGIC_VECTOR(0 TO 107) := (OTHERS => '0'); -- 108

BEGIN

structure : PROCESS (clk, n_reset)
    BEGIN

    if n_reset = '0' then
        
        s_led_array <= (OTHERS => '0');

        elsif rising_edge (clk) then
            
            -- BRICKS ----------------------------------------------------------
            -- block 1
            IF brick_loc_valid_arr(0) = '1' THEN
                s_led_array(coord_to_ind(TO_INTEGER(UNSIGNED(brick_loc_x_arr(0))), TO_INTEGER(UNSIGNED(brick_loc_y_arr(0))))) <= '1';
                s_led_array(coord_to_ind(TO_INTEGER(UNSIGNED(brick_loc_x_arr(0))) + 1, TO_INTEGER(UNSIGNED(brick_loc_y_arr(0))))) <= '1';
            END IF;

            -- block 2
            IF brick_loc_valid_arr(1) = '1' THEN
                s_led_array(coord_to_ind(TO_INTEGER(UNSIGNED(brick_loc_x_arr(1))), TO_INTEGER(UNSIGNED(brick_loc_y_arr(1))))) <= '1';
                s_led_array(coord_to_ind(TO_INTEGER(UNSIGNED(brick_loc_x_arr(1))) + 1, TO_INTEGER(UNSIGNED(brick_loc_y_arr(1))))) <= '1';
            END IF;

            -- block 3
            IF brick_loc_valid_arr(2) = '1' THEN
                s_led_array(coord_to_ind(TO_INTEGER(UNSIGNED(brick_loc_x_arr(2))), TO_INTEGER(UNSIGNED(brick_loc_y_arr(2))))) <= '1';
                s_led_array(coord_to_ind(TO_INTEGER(UNSIGNED(brick_loc_x_arr(2))) + 1, TO_INTEGER(UNSIGNED(brick_loc_y_arr(2))))) <= '1';
            END IF;

            -- block 4
            IF brick_loc_valid_arr(3) = '1' THEN
                s_led_array(coord_to_ind(TO_INTEGER(UNSIGNED(brick_loc_x_arr(3))), TO_INTEGER(UNSIGNED(brick_loc_y_arr(3))))) <= '1';
                s_led_array(coord_to_ind(TO_INTEGER(UNSIGNED(brick_loc_x_arr(3))) + 1, TO_INTEGER(UNSIGNED(brick_loc_y_arr(3))))) <= '1';
            END IF;
            --------------------------------------------------------------------

            -- WALLS -----------------------------------------------------------
            FOR i IN 0 TO 7 LOOP
                s_led_array(coord_to_ind(0, i)) <= '1';
                s_led_array(coord_to_ind(11, i)) <= '1';
            END LOOP;

            s_led_array(coord_to_ind(1, 0)) <= '1';
            s_led_array(coord_to_ind(10, 0)) <= '1';

            FOR i IN 0 TO 11 LOOP
                s_led_array(coord_to_ind(i, 8)) <= '1';
            END LOOP;
            --------------------------------------------------------------------

            -- BALL ------------------------------------------------------------
            s_led_array(coord_to_ind(ball_loc_x, ball_loc_y)) <= '1';
            --------------------------------------------------------------------

            -- PADDLE ----------------------------------------------------------
            s_led_array(coord_to_ind(TO_INTEGER(UNSIGNED(paddle_loc_x)) - 1, 0)) <= '1';
            s_led_array(coord_to_ind(TO_INTEGER(UNSIGNED(paddle_loc_x)), 0)) <= '1';
            s_led_array(coord_to_ind(TO_INTEGER(UNSIGNED(paddle_loc_x)) + 1, 0)) <= '1';
            --------------------------------------------------------------------
    end if ;

    END PROCESS structure;

    led_array <= s_led_array;
   
END rtl;