ARCHITECTURE rtl OF paddle IS

SIGNAL s_pad_loc_x : INTEGER;

BEGIN

paddle_ctrl : process( clk, n_reset ) is

begin
    if n_reset = '0' then
        s_pad_loc_x <= 5;
        elsif rising_edge(clk) then
            if s_pad_loc_x != 3 AND s_pad_loc_x != 8 then
            if but_left = '1' then
                s_pad_loc_x <= TO_INTEGER(to_unsigned(s_pad_loc_x)) - 1;
            elsif but_right = '1' then
                s_pad_loc_x <= TO_INTEGER(to_unsigned(s_pad_loc_x)) + 1;
            end if ;
            if but_left = '1' AND but_right = '1' then
                s_pad_loc_x <= s_pad_loc_x;
            end if ;
        end if;
    end if ;

end process paddle_ctrl; -- paddle_ctrl

paddle_loc_x <= TO_UNSIGNED(UNSIGNED(s_pad_loc_x), 4);

END rtl;