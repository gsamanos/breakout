ARCHITECTURE rtl OF timer IS

SIGNAL s_counter : INTEGER := count - 1;
BEGIN
    timer : PROCESS (clk, n_reset)
    BEGIN
    IF n_reset = '0' THEN s_counter <= 0;
        ELSIF rising_edge(clk) THEN
            IF reset = '1' THEN
                s_counter <= 0;
            ELSE
        IF s_counter = count - 1 THEN s_counter <= 0;
        ELSE s_counter <= s_counter + 1;
        END IF;
            END IF;
        END IF;
    END PROCESS timer;

    flag <= '1' WHEN s_counter = count - 1 ELSE '0';

END rtl;